Name:           acrn-board-inspector
Version:        3.2
Release:        1%{?dist}
Summary:        Generate Board Configuration for ACRN
License:        BSD-3-Clause
URL:            https://projectacrn.org/
Source0:        https://codeload.github.com/projectacrn/acrn-hypervisor/tar.gz/refs/tags/v3.2

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description
This package collects all board related info to generate a board configuration
file for ACRN.

%package -n python3-%{name}
Summary: Generate Board Configuration for ACRN
Requires: cpuid msr-tools pciutils usbutils dmidecode python3-lxml python3-xmlschema python3-tqdm
%description -n python3-acrn-board-inspector
This package collects all board related info to generate a board configuration
file for ACRN.

%prep
%autosetup -n acrn-hypervisor-%{version}

%build
cd debian/acrn-board-inspector
ACRNVERSION=%{version} %py3_build

%install
cd debian/acrn-board-inspector
ACRNVERSION=%{version} %py3_install

%files -n python3-acrn-board-inspector
%{_bindir}/acrn-board-inspector
%{python3_sitelib}/acrn_board_inspector*

%changelog
* Thu May 02 2024 Erico Nunes <ernunes@redhat.com>
- Initial package
